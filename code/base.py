import numpy as np
import sys

import re
import os
import pandas as pd
import matplotlib.pyplot as plt
import pywt
import dtw

from sklearn.linear_model import HuberRegressor, Ridge, Lasso


def _dtw_distance(ts_a, ts_b, max_warping_window=100, d=lambda x, y: abs(x-y)):
    """Returns the DTW similarity distance between two 2-D
    timeseries numpy arrays.

    Arguments
    ---------
    ts_a, ts_b : array of shape [n_samples, n_timepoints]
        Two arrays containing n_samples of timeseries data
        whose DTW distance between each sample of A and B
        will be compared

    max_warping_window : int, optional (default = infinity)
        Maximum warping window allowed by the DTW dynamic
        programming function

    d : DistanceMetric object (default = abs(x-y))
        the distance measure used for A_i - B_j in the
        DTW dynamic programming function

    Returns
    -------
    DTW distance between A and B
    """

    # Create cost matrix via broadcasting with large int
    ts_a, ts_b = np.array(ts_a), np.array(ts_b)
    M, N = len(ts_a), len(ts_b)
    cost = sys.maxsize * np.ones((M, N))

    # Initialize the first row and column
    cost[0, 0] = d(ts_a[0], ts_b[0])

    for i in range(1, M):
        cost[i, 0] = cost[i-1, 0] + d(ts_a[i], ts_b[0])

    for i in range(1, N):
        cost[0, i] = cost[0, i-1] + d(ts_a[0], ts_b[i])

    # Populate rest of cost matrix within window
    for i in range(1, M):
        for j in range(max(1, i - max_warping_window),
                       min(N, i + max_warping_window)):
            choices = cost[i - 1, j - 1], cost[i, j - 1], cost[i - 1, j]
            cost[i, j] = min(choices) + d(ts_a[i], ts_b[j])

    # Return DTW distance given window
    return cost


def _dtw_path(dis_m):
    """
    :param dis_m:
    :return:
    """
    l_ind = np.array([0, 0])
    max_v = np.max(dis_m)
    ind = [l_ind]

    for i in range(max(dis_m.shape)):
        dis_m[l_ind[0], l_ind[1]] = max_v
        # dis_m[l_ind[0], l_ind[1]] = max_v  # нововведение !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        local_m = dis_m[l_ind[0]: l_ind[0] + 2, l_ind[1]: l_ind[1] + 2]

        l_ind += np.unravel_index(local_m.argmin(), local_m.shape)
        ind.append(l_ind)

    return list(zip(*ind))


class StaticFunc:

    @staticmethod
    def norm_transform(x):
        """
        :param x:
        :return:
        """
        return (x - np.min(x)) / (np.max(x) - np.min(x))

    @staticmethod
    def rss_me(first_ts, second_ts):
        """
        :param first_ts:
        :type first_ts: pandas.Series

        :param second_ts:
        :type second_ts: pandas.Series

        :return:
        """
        # assert (first_ts.index == second_ts.index)
        return np.sum((first_ts - second_ts)**2)
        # TODO: assert !!!


class DtwStep(StaticFunc):
    def __init__(self, model_s, class_s, start_l,
                 end_l, _reg=HuberRegressor(), with_norm=True):
        """
        :param model_s:
        :type model_s pandas.Series

        :param class_s:
        :type class_s: pandas.Series

        :param start_l:
        :type start_l: int

        :param end_l:
        :type end_l: int

        :param _reg:
        :type _reg:

        :param with_norm:
        :type with_norm: bool
        """
        self.model_s = model_s

        if with_norm:
            self.class_s = self.norm_transform(class_s)
        else:
            self.class_s = class_s

        self.start_l = start_l
        self.end_l = end_l
        self._reg = _reg

    @property
    def _dtw_step(self):
        """
        :return:
        """
        model_cut_s = self.model_s.iloc[self.start_l:self.end_l]
        dist, acc, path = dtw.dtw(model_cut_s, self.class_s, dist=lambda x, y: abs(x - y))

        return dist, acc, path

    def __get_weights(self, mode):
        """
        :return:
        """
        # TODO: добавить в функцию параметр вида весов
        # TODO: попробовать с локальными максимумами ??????

        if mode == "equal":
            return pd.Series(1, self.model_s.index)

        if mode == "linear":
            return self.norm_transform(self.model_s).to_dict()

    def _coeff_step(self, mode):
        """
        :return:
        """
        dist, acc, path = self._dtw_step

        x = (path[0][self.start_l:self.end_l]).reshape(-1, 1)
        x = np.hstack([x * 0 + 1, x])
        y = path[1][self.start_l:self.end_l]

        w_dict = self.__get_weights(mode)
        w_array = np.array([w_dict[i] for i in y])  # sigmoid ???

        self._reg.fit(X=x, y=y, sample_weight=w_array)
        shift_coef, stretch_coef = self._reg.coef_
        stretch_coef = 1 / stretch_coef
        return shift_coef, stretch_coef

    def _linear_step(self, mode):
        """
        :return:
        """
        # TODO: возвращать также RMSE !!!

        class_dtw = self.class_s.copy()
        shift_coef, stretch_coef = self._coeff_step(mode)

        class_dtw.index = stretch_coef * class_dtw.index
        class_dtw.index -= shift_coef
        class_dtw.index = np.round(class_dtw.index).astype(int)

        print(self._reg.coef_)
        return class_dtw

    def __call__(self, *args, **kwargs):
        return self._linear_step(*args, **kwargs)


def semor_step(*, model_s, class_s):
    """
    :param model_s:
    :type model_s: pandas.Series

    :param class_s:
    :type class_s: pandas.Series
    :return:
    """

    class_s = class_s[~class_s.index.duplicated(keep="last")]
    class_s = class_s.reindex(model_s.index)

    class_s.fillna(method="bfill", inplace=True)
    class_s.fillna(method="ffill", inplace=True)

    x = class_s.values.reshape(-1, 1)
    x = np.hstack([x * 0 + 1, x])

    y = model_s.values

    reg = Ridge(alpha=0)
    reg.fit(X=x, y=y)

    coef_ = reg.coef_
    print(coef_)

    new_class_s = coef_[0] + coef_[1] * class_s
    return new_class_s, StaticFunc.rss_me(new_class_s, model_s)


