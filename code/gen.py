import numpy as np


class MakePopulation:
    """
    Класс для сэмплирования
    """
    def __init__(self, acc, epsilon):
        """
        TODO: посмотреть как называется матрица acc
        :param acc: matrix
        axe_0 = model length
        axe_1 = segment length
        axe_0 < axe_1
        :type acc: numpy.ndarray
        :param epsilon:
        :type epsilon: float
        """
        self.__acc = acc
        self.__shape_0, self.__shape_1 = self.__acc.shape
        self._sum = None
        self.epsilon = epsilon

    def make_population(self, p_size):
        """
        :param p_size:
        :return: y_left, y_right,
        """
        p_x = np.random.randint(self.__shape_1, size=(p_size, 2))
        # p_y = np.random.randint(self.__acc.shape[0], size=(p_size, 1))
        # p = np.hstack([np.sort(p_x, axis=1), p_y])

        return np.sort(p_x, axis=1)  # пока рассмотрим двухпараметрическое сэмплирование

    @staticmethod
    def make_crossover(p_array, c_size):
        """
        стадия скрещивания
        просто усредняем сэмплы
        :return:
        """
        # рандомно выбираем c_size пар из предыдущего
        # и поулчаем столько новых особей
        c_ind = np.random.randint(p_array.shape[0], size=(c_size, 2))
        c_array = np.array([0.5 * (p_array[i[0]] + p_array[i[1]]) for i in c_ind])

        return c_array.astype(int)

    def __make_one_mutation(self, y):
        """
        :param y:
        :return:
        """
        y_left, y_right = y
        r_border = -y_left, y_right - y_left
        y_left += np.random.randint(int(r_border[0]/2), int(r_border[1]/2))

        r_border = y_left - y_right, self.__shape_1 - y_right
        y_right += np.random.randint(int(r_border[0]/2), int(r_border[1]/2))

        return np.array([y_left, y_right])

    def make_mutation(self, p_array):
        """
        стадия мутации
        :param p_array:
        :return:
        """
        for ind in range(p_array.shape[1]):
            p_array[ind, :] = self.__make_one_mutation(p_array[ind, :])

        return p_array

    def _make_one_est(self, y):
        """
        :param y:
        :return:
        """
        y_left, y_right = y

        # line_0 = np.vstack([np.zeros(y_left), np.arange(y_left)])

        der = (y_right - y_left)/self.__shape_0
        line_1x = np.arange(self.__shape_0)
        line_1y = der*line_1x + y_left
        line_1y = line_1y.astype(int)
        line_1 = np.vstack([line_1x, line_1y])

        # line_2 = np.vstack([self.__shape_0*np.ones(self.__shape_1 - y_right),
        #                     np.arange(y_right, self.__shape_1)])

        # path = np.hstack([line_0, line_1, line_2]).astype(int)

        return line_1

    def make_estimation(self, p_array, sample_size):
        """
        стадия вычисления полезности особи
        :param p_array:
        :param sample_size:
        :return:
        """
        sample_x = np.random.randint(self.__shape_0, size=sample_size)

        all_sum = []

        for ind in np.arange(p_array.shape[0]):
            local_line = self._make_one_est(p_array[ind, :])
            local_line = [self.__acc[local_line[0, i], local_line[1, i]] for i in sample_x]
            all_sum.append(np.sum(local_line))
        return p_array, all_sum

    @staticmethod
    def make_selection(p_array, all_sum, select_size):
        """
        стадия отбора
        :param p_array:
        :param all_sum:
        :param select_size:
        :return:
        """
        ind = np.argsort(all_sum)[:select_size]

        p_array = np.take(p_array, ind, axis=0)
        min_pair = (all_sum[ind[0]], p_array[0])
        return p_array, min_pair, all_sum

    def make_stop(self, min_pair, all_sum):
        """
        # TODO: Текущий критерий остановки плохой. Нужно менять либое его либо алгоритм
        :param min_pair:
        :param all_sum:
        :return:
        """
        if self._sum is None:
            self._sum = [np.mean(all_sum)]
        else:
            self._sum.append(np.mean(all_sum))

        if self._sum[-1] / np.mean(self._sum) < self.epsilon:
            return min_pair, self._sum


class EvolutionProcess(MakePopulation):
    """
    Класс для непосредственного запуска эволюционного алгоритма
    """
    def __init__(self, acc, epsilon):
        """
        :param acc:
        :param epsilon:
        """
        super().__init__(acc, epsilon)

    def run_forest(self, p_size, c_size, sample_size, select_size):
        """
        :param p_size:
        :type p_size: int

        :param c_size:
        :type c_size: int

        :param sample_size:
        :type sample_size: int

        :param select_size:
        :type select_size: int

        :return:
        """
        p_array = self.make_population(p_size)

        while True:
            p_array = self.make_crossover(p_array, c_size)
            p_array = self.make_mutation(p_array)
            p_array, all_sum = self.make_estimation(p_array, sample_size)
            p_array, min_pair, all_sum = self.make_selection(p_array, all_sum, select_size)
            min_pair = self.make_stop(min_pair, all_sum)
            if min_pair:
                return min_pair

    def __call__(self, *args, **kwargs):
        return self.run_forest(*args, **kwargs)

